# README #

Esse arquivo contem instruções de uso de cada um dos scripts gerados

-----------------------------


### crawler_inmet.php ###

Conecta no site do INMET e pega as últimas previsões de todas as estações meteorológicas do Brasil

Para rodar o script, acesse o terminal e digite:  
# php crawler_inmet.php >> file.csv #


-----------------------------


### /nooa/downloadNOOA.php ###

 *      Esse script tem por objetivo conectar no FTP da NOAA e baixar todos os arquivos de previsão de clima de $FILE_DURATION dias passados.
 *      Como os arquivos são grandes, o script também fica responsável pela limpeza do HD, deletando todos os arquivos anteriores a $FILE_DURATION dias.
 *      Para garantir eficiência e economizar banda, o script verifica se o arquivo já existe em ambiente local antes de realizar o download.

Abra o arquivo e altere o valor de $FILE_DURATION caso prefira (default 5 dias)

Para rodar o script, acesse o terminal e digite: 
# php downloadNOOA.php #
Aguarde, esse script pode demorar horas.

-----------------------------

### /getDecks/crawler_ccee.sh ###

 *      Esse script tem por objetivo conectar no site do CCEE e realizar o download do deck Newave e do deck Decomp do mes atual e do mes anterior.
 *      Antes de realizar o download, o script verifica se já existe um arquivo local com o mesmo nome do que será baixado, se for encontrado, o mesmo é deletado.

Para rodar o script, acesse o terminal e digite: 
# bash crawler_ccee.sh
Aguarde, esse script pode demorar alguns minutos

-----------------------------

### DAT_Generator.php ###

Processa os dados das usinas de cada bacia para gerar o arquivo de vazão.

Para rodar o script, acesse o terminal dentro da pasta /DAT_Generator e digite:  
# php DAT_Generator.php #