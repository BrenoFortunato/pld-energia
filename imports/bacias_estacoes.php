<?php
    # Estrutura do arquivo: O Array $input_data contém as informações de todas as bacias
    # que estão sendo trabalhadas além das IDs das estações meteorológicas  de interesse. Este arquivo 
    # deve ser atualizado antes de executar o INMET_Updater.

    # IMPORTANTE: O nome da bacia aqui deve ser igual ao nome da pasta

    $input_data = [
        "Araguari" => [81638],
        "Parana" => [86620,12345],
	"Doce" => [5555,4656]
    ];
