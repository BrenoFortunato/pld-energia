<?php

require("../imports/bacias_estacoes.php");
define("INMET_FILES_DIR", "./INMET_Files/");
define("INTERPLU_FILES_DIR", "INTERPLU_Files/");
define("ALLOW_ERRORS", true);
define("TODAY", getdate());

main();

function get_today_file_name(){
    $filename = "";
    $filename = TODAY['mday'].str_pad(TODAY['mon'], 2, '0', STR_PAD_LEFT).TODAY['year'].".csv";
    return $filename;
}

function check_file_exists($filename){
    $file_path = INMET_FILES_DIR.$filename;
    if(file_exists($file_path)){
        return TRUE;
    } else {
        echo "Arquivo: ".$filename." Não encontrado! Encerrando execução ...\n";
        exit;
    }
}

function show_status($done, $total, $size=30) {
    static $start_time;
    // if we go over our bound, just ignore it
    if($done > $total) return;
    if(empty($start_time)) $start_time=time();
    $now = time();
    $perc=(double)($done/$total);
    $bar=floor($perc*$size);
    $status_bar="\r[";
    $status_bar.=str_repeat("=", $bar);
    if($bar<$size){
        $status_bar.=">";
        $status_bar.=str_repeat(" ", $size-$bar);
    } else {
        $status_bar.="=";
    }
    $disp=number_format($perc*100, 0);
    $status_bar.="] $disp%  $done/$total";
    $rate = ($now-$start_time)/$done;
    $left = $total - $done;
    $eta = round($rate * $left, 2);
    $elapsed = $now - $start_time;
    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";
    echo "$status_bar  ";
    flush();
    // when done, send a newline
    if($done == $total) {
        echo "\n\n";
    }

}

function main(){
    global $input_data;
    $filename = get_today_file_name();
    if(check_file_exists($filename)){
     
        #echo "Iniciado processamento do arquivo com dados do dia: ".$filename."\n";	
     
        $handle = fopen(INMET_FILES_DIR.$filename,'r');
        $data = fgetcsv($handle,0,','); //Pula cabeçalho! Não remova!
        $acumulado = [];

        //zerando todas as estações de interesse
        foreach($input_data as $key => $bacia){
            foreach ($bacia as $id => $station){
                $acumulado = initialize($acumulado, $station); 
            }
        }

        while ( ($data = fgetcsv($handle,0,',') ) !== FALSE ) {
                           
            //Processa 'tempMax'
            $acumulado[$data[1]] = accumulate($data[3],$acumulado[$data[1]],'tempMax','qtdTempMax');
            //Processa 'tempMin'
            $acumulado[$data[1]] = accumulate($data[4],$acumulado[$data[1]],'tempMin','qtdTempMin');
            //Processa 'umidade'
            $acumulado[$data[1]] = accumulate($data[5],$acumulado[$data[1]],'umidade','qtdUmidade');
            //Processa 'pressao'
            $acumulado[$data[1]] = accumulate($data[6],$acumulado[$data[1]],'pressao', 'qtdPressao');
            //Processa 'precipitacao'
            $acumulado[$data[1]] = accumulate($data[7],$acumulado[$data[1]],'precipitacao');
            //Processa 'ventoDir'
            $acumulado[$data[1]] = accumulate($data[8],$acumulado[$data[1]],'ventoDir','qtdVentoDir');
            //Processa 'ventoVel'
            $acumulado[$data[1]] = accumulate($data[9],$acumulado[$data[1]],'ventoVel','qtdVentoVel');
        }

        foreach($acumulado as $key => $temp){
            $acumulado[$key] = calc_average($temp);
        }    
        
        foreach($input_data as $key => $bacia){            
            if (!file_exists(INTERPLU_FILES_DIR.$key)) {
                echo "Pasta inexistente! Criando uma nova pasta ... ";
                mkdir(INTERPLU_FILES_DIR.$key, 0777, true);
                echo "OK\n";
            }

            foreach ($bacia as $id => $station){
                $file = INTERPLU_FILES_DIR.$key . "/" . str_pad($station, 8, '0', STR_PAD_LEFT) . ".txt";
                $info = "";

                if (!file_exists($file)) {
                    $station_file = fopen($file, "w");
                } else {
                    $station_file = fopen($file, "a");              
                    if( substr(getLastLine($file), -1) != "\n"){
                        $info = "\n";
                    }
                }
                if(array_key_exists($station, $acumulado)){
                
                    if (array_sum($acumulado[$station]) <= 0){
                        $value = "-1.00";
                    } else {
                        $value = number_format($acumulado[$station]["precipitacao"], 2, '.', '');
                    }

                    $info .= str_pad(TODAY['mday'], 6, ' ', STR_PAD_LEFT).
                            str_pad(TODAY['mon'], 6, ' ', STR_PAD_LEFT).
                            str_pad(TODAY['year'], 6, ' ', STR_PAD_LEFT).
                            str_pad($value, 12, ' ', STR_PAD_LEFT);
                    fwrite($station_file,$info);
                    fclose($station_file);
                    
                 }        
            }   
        }       
    } else {
            echo "Arquivo do dia não encontrado!\n";
    }

    $key = 'Araguari';
    $file = INTERPLU_FILES_DIR.$key."/".str_pad(81638, 8, '0', STR_PAD_LEFT).".txt";
    $pathInterplu = INTERPLU_FILES_DIR.$key."/Interplu.hig";       
    processaTxt($pathInterplu, $file);


}

function calc_average($station){    
    if ($station['tempMax'] != -99999 && $station['qtdTempMax'] != 0){
        $station['tempMax'] = $station['tempMax']/$station['qtdTempMax'];
    } else {
        $station['tempMax'] = -1;
    }
    if ($station['tempMin'] != -99999 && $station['qtdTempMin'] != 0){
        $station['tempMin'] = $station['tempMin']/$station['qtdTempMin'];
    } else {
        $station['tempMin'] = -1;
    }
    if ($station['umidade'] != -99999 && $station['qtdUmidade'] != 0){
        $station['umidade'] = $station['umidade']/$station['qtdUmidade'];
    } else {
        $station['umidade'] = -1;
    }
    if ($station['pressao'] != -99999 && $station['qtdPressao'] != 0){
        $station['pressao'] = $station['pressao']/$station['qtdPressao'];
    } else {
        $station['pressao'] = -1;
    }
    if ($station['ventoDir'] != -99999 && $station['qtdVentoDir'] != 0){
        $station['ventoDir'] = $station['ventoDir']/$station['qtdVentoDir'];
    } else {
        $station['ventoDir'] = -1;
    }    
    if ($station['ventoVel'] != -99999 && $station['qtdVentoVel'] != 0){
        $station['ventoVel'] = $station['ventoVel']/$station['qtdVentoVel'];
    } else {
        $station['ventoVel'] = -1;
    }
    if ($station['precipitacao'] == -99999){
        $station['precipitacao'] = -1;
    }
    return $station;
}

function accumulate($temp_var,$tempAcumulado,$index,$counter = null){

    if(!ALLOW_ERRORS){
        if($tempAcumulado[$index] != -99999){
            if (is_numeric($temp_var)){
                $tempAcumulado[$index] += $temp_var;
                if ($counter != null)
                    $tempAcumulado[$counter]++;
            } else {
                if (strpos($temp_var, '/') !== true) {
                    echo "WARNING: Invalid data detected! Found : ".$temp_var." It should be a number!\n";
                    echo "ACTION: All '".$index."' data from this station will be ignored today!\n";
                    $tempAcumulado[$index] = -99999;                        
                    if ($counter != null)
                        $tempAcumulado[$counter] = -99999;
                }
            }
        }
    } else {
        if (is_numeric($temp_var)){
            $tempAcumulado[$index] += $temp_var;
            if ($counter != null)
                $tempAcumulado[$counter]++;
        } else {
            if (strpos($temp_var, '/') !== true) {
                echo "WARNING: Invalid data detected! Found : ".$temp_var." It should be a number!\n";
            }
        }
    }

    return $tempAcumulado;
}

function initialize($acumulado, $index){
    if(!isset($acumulado[$index])){
        $acumulado[$index]['tempMax'] = 0; 
        $acumulado[$index]['tempMin'] = 0; 
        $acumulado[$index]['umidade'] = 0; 
        $acumulado[$index]['pressao'] = 0; 
        $acumulado[$index]['precipitacao'] = 0; 
        $acumulado[$index]['ventoDir'] = 0; 
        $acumulado[$index]['ventoVel'] = 0; 
        $acumulado[$index]['qtdTempMax']=0;
        $acumulado[$index]['qtdTempMin']=0;
        $acumulado[$index]['qtdPressao']=0;
        $acumulado[$index]['qtdUmidade']=0;
        $acumulado[$index]['qtdVentoDir']=0;
        $acumulado[$index]['qtdVentoVel']=0; 
    }
    return $acumulado;
}
//pega o arquivo txt passado como parametro, calcula a diferenca entre a primeira e ultima data e atualiza o arquivo interplu.hig
function processaTxt($pathInterplu, $txtPath){
	$linhaInicial = getFirstLine($txtPath);
	$diaInicial = (int)trim(substr($linhaInicial, 0, 6));
	$mesInicial = (int)trim(substr($linhaInicial, 6, 6));
	$anoInicial = (int)trim(substr($linhaInicial, 12, 6));

	$linhaFinal = getLastLine($txtPath);
	$diaFinal = (int)trim(substr($linhaFinal, 0, 6));
	$mesFinal = (int)trim(substr($linhaFinal, 6, 6));
	$anoFinal = (int)trim(substr($linhaFinal, 12, 6));

	$dataInicial = new DateTime();
    $dataInicial->setDate($anoInicial, $mesInicial, $diaInicial);
	$dataFinal = new DateTime();
    $dataFinal->setDate($anoFinal, $mesFinal,$diaFinal);

	$intervalo = $dataFinal->diff( $dataInicial );

	atualizaInterpluHig($pathInterplu, $diaFinal,$mesFinal,$anoFinal,$intervalo->days);
}

function getLastLine($txtPath){
	$file = file($txtPath);
    $lastLine = $file[count($file)-1];
    return $lastLine;
    
}

function getFirstLine($txtPath){
	//dd($txtPath);
    $file = file($txtPath);
	$lastLine = $file[0];
	return $lastLine;
}

function atualizaInterpluHig($pathInterplu, $dia, $mes, $ano, $nt){

	//abre o arquivo
    $file = fopen($pathInterplu, "a");
	if($file){
		$conteudo = file_get_contents($pathInterplu);
		//decodifica para facilitar o trabalho
		$conteudo = mb_convert_encoding($conteudo, 'UTF-8', 'ISO-8859-16');
		//explode o arquivo para trabalhar com as linhas separadamente
		$linhas = explode("\n", $conteudo);


		//gera a nova linha 5 com as datas passadas como parametro
		$novaLinha5 = completaEspaco($dia, 10) . completaEspaco($mes, 10) . completaEspaco($ano, 10);


		//armazena valores antigos da linha 7
		$NCNU = substr($linhas[7], 0, 20); 
		$NP = substr($linhas[7], 30, 10);

		//gera o novo número de dias, com seus respectivos espaços a esquerda
		$NT = completaEspaco($nt, 10);

		//atualiza a linha 7 com o novo NT, mantendo os valores antigos inalterados
		$novaLinha7 = $NCNU . $NT . $NP; 

		//atualiza as linhas 5 e 7 com seus novos valores
		$linhas[5] = $novaLinha5;
		$linhas[7] = $novaLinha7;

		//gera o novo conteúdo
		$novoConteudo = "";
		foreach ($linhas as $linha) {
			$novoConteudo .= $linha . "\n";
		}

		//codifica para o encoding original
		$novoConteudo = mb_convert_encoding($novoConteudo, 'ISO-8859-16', 'UTF-8');

		//salva o arquivo (por enquanto com o prefixo novo_ - Favor remover depois)
		file_put_contents($pathInterplu.".novo", $novoConteudo);

	}else
		echo 'arquivo não encontrado';

	fclose($file);
}

//completa com espacos a esquerda
	//$string - valor inicial
	//$num - número de caracteres que a string deverá ter
function completaEspaco($string, $num){
	return str_pad($string, $num, " ", STR_PAD_LEFT);
}

//dump and die - apenas para debug
function dd($var){
	if(is_array($var)){
		print_r($var);
	}elseif (is_string($var)) {
		echo $var;
	}
	else{
		var_dump($var);
	}
	exit;
}

//função útil para identificar o encodig do arquivo analisado
function checaEncoding($str){
	foreach(mb_list_encodings() as $chr){ 
	    echo mb_convert_encoding($str, 'UTF-8', $chr)." : ".$chr."<br>";    
	}
}