<?php

define("DATASET_DATSORIGINAIS", "./dataset_datsoriginais");
define("DATASET_DATSGERADOS", "./dataset_datsgerados");
define("DATASET_CSVFILES", "./dataset_csvfiles/");
define("DATASET_HIGFILES", "./dataset_higfiles/"); //nome_bacia/hist_rcps/anos/hadgem_miroc.hig
define("ANO_INICIAL_VAZOES", "1931");

main();

/* @param   int     $done   how many items are completed
 * @param   int     $total  how many items are to be done total
 * @param   int     $size   optional size of the status bar
 * @return  void
*/
function show_status($done, $total, $size=30) {
    static $start_time;
    // if we go over our bound, just ignore it
    if($done > $total) return;
    if(empty($start_time)) $start_time=time();
    $now = time();
    $perc=(double)($done/$total);
    $bar=floor($perc*$size);
    $status_bar="\r[";
    $status_bar.=str_repeat("=", $bar);
    if($bar<$size){
        $status_bar.=">";
        $status_bar.=str_repeat(" ", $size-$bar);
    } else {
        $status_bar.="=";
    }
    $disp=number_format($perc*100, 0);
    $status_bar.="] $disp%  $done/$total";
    $rate = ($now-$start_time)/$done;
    $left = $total - $done;
    $eta = round($rate * $left, 2);
    $elapsed = $now - $start_time;
    $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";
    echo "$status_bar  ";
    flush();
    // when done, send a newline
    if($done == $total) {
        echo "\n\n";
    }

}

function main(){
	$modelos_climaticos = array("hadgem", "miroc");
	$cenarios = array("historico", "rcp85", "rcp45");
	$periodos = array("2011-2040", "2041-2070", "2071-2099");

	foreach($modelos_climaticos as $modelo){
		foreach($cenarios as $cenario){		
			if ($cenario == "historico"){
				ProcessaArquivo($modelo, $cenario, "");
				continue;
			}
			foreach($periodos as $periodo){
				ProcessaArquivo($modelo, $cenario, $periodo);
			}
		}
	}
}

#Função de processamento
function ProcessaArquivo($modelo, $cenario, $periodo){
	$locale_info = localeconv();
	$separador = $locale_info['decimal_point'];
	$nome_arquivo_vazao_original = "";

	if($cenario == "historico"){
		$nome_arquivo_vazao_original = DATASET_DATSORIGINAIS."/".$cenario."/vazoes.dat";
	} else {
		$nome_arquivo_vazao_original = DATASET_DATSORIGINAIS."/".$periodo."/vazoes.dat";
	}

	if(!file_exists($nome_arquivo_vazao_original)){
		return;
	}
	$nome_arquivo_modelo = "";
	if ($cenario == "historico") {
		$nome_arquivo_modelo = DATASET_CSVFILES.$modelo."_".$cenario.".csv";
	} else {
		$nome_arquivo_modelo = DATASET_CSVFILES.$modelo."_".$cenario."_".$periodo.".csv";
	}

	if(!file_exists($nome_arquivo_modelo)){ 
		return;
	}

	$postos = array();
	for($i = 0; $i < 600; $i++){
		$postos[$i] = array();
	}

	#Leitura dos arquivos binários
	echo "-----------------------------------------------";
	echo "\nProcessando arquivo de vazões originais: ".$nome_arquivo_vazao_original."\n";
	$handle = fopen($nome_arquivo_vazao_original,'rb');
	$fsize = filesize($nome_arquivo_vazao_original); 
	$contents = fread($handle, $fsize); 
	$byteArray = unpack("l*", $contents); 
	$arrayvalues = array_values($byteArray);

	$i = 0;
	while($i < sizeof($arrayvalues)){ 
		for($j = 0; $j < 600; $j++){
			array_push($postos[$j],$arrayvalues[$i]);
			$i++;
		}
		show_status($i,sizeof($arrayvalues));
	}
	fclose($handle);
	
	echo "Iniciado processamento dos arquivos CSV: ".$nome_arquivo_modelo."\n";	
	//Quantidade de linhas do arquivo csv:
	$size_arquivo_modelo = count(file($nome_arquivo_modelo))+1;
	echo "Quantidade de linhas no arquivo .csv: ".($size_arquivo_modelo-1)."\n";
	$csv_counter = 2;

	$handle = fopen($nome_arquivo_modelo,'r');
	//Essa linha abaixo serve para pular a primeira linha do arquivo que contém o cabeçalho do CSV! DO NOT REMOVE IT!
	$data = fgetcsv($handle,0,',');
	
	//O laço abaixo irá iterar dentro de cada linha do arquivo .csv montado acima e armazenado em $nome_arquivo_modelo
	while ( ($data = fgetcsv($handle,0,';') ) !== FALSE ) {
		$data = array_map("utf8_encode", $data); 
		$nome_arquivo_temp = DATASET_HIGFILES.$data[5];
		$coluna = $data[6];
	
		$posto = -1;
		try {
			$posto = $data[1];
		} catch (Exception $e){
			continue;
		} 

		$handleTemp = fopen($nome_arquivo_temp, 'r');
		
		$hig_counter = 1;
		$hig_counter++;
		$registro = (2011 - ANO_INICIAL_VAZOES) * 12;
		if ($handleTemp != null) {
			
			$size_handleTemp = count(file($nome_arquivo_temp));
			$size_handleTemp++;
			echo $nome_arquivo_temp."\n";
			while (($line = fgets($handleTemp)) !== false) {
				show_status($hig_counter,$size_handleTemp);
				$campos = array_values(array_filter(explode(" ", $line)));				

				if (strlen($campos[0]) > 7){
					$aux_campos = $campos[0];
					$aux_campos = array_values(array_filter(explode("\t", $aux_campos)));
					unset($campos[0]);
					$result = array_merge($aux_campos,$campos);
					$campos = array_values($result);
				}

				//DEBUG TOOL : Escolha o arquivo e a faixa de posição do arquivo hig
				#if (strpos($nome_arquivo_temp, 'hadgem')){
					
				#	if($hig_counter >0 && $hig_counter < 5){
				#		print_r($campos);

				#	}
				#} 
				$indice = 2 + $coluna;
				if ($indice > (sizeof($campos) - 1)){
					$indice = sizeof($campos) - 1;
				}

				$dado = $campos[$indice];
				$dado = str_replace(".", $separador, $dado);
				$vazao = floatval($dado);	

				if ($registro >= sizeof($postos[$posto -1])){
					continue;
				}
				$postos[$posto - 1][$registro++] = $vazao;
				$hig_counter++;
			}
			fclose($handleTemp);
		} else {
			echo "Failed\n";
		}
		$csv_counter++;
	}


	echo "\n\nStatus do processamento do arquivo CSV: ".$nome_arquivo_modelo."\n";	
	show_status($csv_counter,$size_arquivo_modelo);
	fclose($handle);

	//Checa se existe diretrio e se não existe cria!
	if (!file_exists(DATASET_DATSGERADOS)) {
		mkdir(DATASET_DATSGERADOS, 0777, true);
	}

	//Escrever os arquivos .dat 
	$nome_arquivo_saida = "";
	if ($cenario == "historico"){
		$nome_arquivo_saida = DATASET_DATSGERADOS."/".$modelo."_".$cenario.".dat";
	} else {
		$nome_arquivo_saida = DATASET_DATSGERADOS."/".$modelo."_".$periodo."_".$cenario.".dat";		
	}

	echo "Escrevendo arquivo .DAT:\n";
	$fp = fopen($nome_arquivo_saida, "w");
	$i = 0;
	while($i < sizeof($postos[0])){ 
		for($j = 0; $j < 600; $j++){
			#echo "i: ".$i." - "."j: ".$j." - ".$postos[$j][$i]."\n";
			$my_byte_array = pack("l*",$postos[$j][$i]);
			fwrite($fp, $my_byte_array);
		}
		$i++;
		show_status($i,sizeof($postos[0]));
	}
	fclose($fp);
}

?>