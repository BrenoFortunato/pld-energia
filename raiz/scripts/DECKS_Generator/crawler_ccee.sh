#!/bin/sh


# Esse script tem como objetivo capturar automaticamente os decks do NeWave e do DECOMP do site da CCEE
#	Funcionamento:
#		A cada $DECOMP_TIME o sistema conectará no site da CCEE e baixará o deck do DECOMP mais atualizado disponível
#		A cada $NEWAVE_TIME o sistema conectará no site da CCEE e baixará o deck do NEWAVE mais atualizado disponível


# Funcao para download
Download () {
	# Monta o nome do arquivo para download no servidor e armazenamento local
	arquivo="$sigla$ano$mes"

	# Verfica se existe o arquivo no diretorio local
	if [ -f "$arquivo" ]
	then
		# Se encontrou, remove 
		rm "$arquivo"
		echo ">>>>>>>>>> $arquivo encontrado e removido"
	else
		# Senao, so informa que nao achou
		echo ">>>>>>>>>> $arquivo nao encontrado"
	fi

	# Realiza o download
	echo ">>>>>>>>>> Iniciando download de $arquivo"
	wget "https://www.ccee.org.br/ccee/documentos/$arquivo"
	echo ">>>>>>>>>> Download encerrado"
}


# Obtendo mes atual
mes="$(date +'%m')"
#mes="01"

# Obtendo ano atual
ano="$(date +'%Y')"

# Obtendo arquivo para NW
sigla="NW"

# Chama a funcao para realizar download do NW para o mes atual
Download $sigla $ano $mes

# Obtendo arquivo para DC
sigla="DC"

# Chama a funcao para realizar download do DC para o mes atual
Download $sigla $ano $mes

# Se o mes atual for 1
if [ "$(($mes - 1))" = "0" ]
then
	# Entao o mes anterior eh 12 e ano=ano-1
	mes="12"
	ano="$(($ano - 1))"
else
	# Senao, se o mes atual for maior ou igual a 10
	if [ "$(($mes - 1))" -ge "10" ]
	then
		# Simplesmnete faz mes=mes-1
		mes="$(($mes - 1))"
	else
		# Senao coloca 0 e faz mes=mes-1
		mes="0$(($mes - 1))"
	fi
fi

# Obtendo arquivo para NW
sigla="NW"

# Chama a funcao para realizar download do NW para o mes anterior
Download $sigla $ano $mes

# Obtendo arquivo para DC
sigla="DC"

# Chama a funcao para realizar download do DC para o mes anterior
Download $sigla $ano $mes