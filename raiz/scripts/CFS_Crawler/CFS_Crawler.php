<?php
    // Base URL
    $BASE_URL = "http://nomads.ncep.noaa.gov/pub/data/nccf/com/cfs/prod/";

    // Get (current date - 6 days)
    $date = date("Ymd", strtotime('-6 days'));

    // Array to store folders that should not be deleted
    $folders_to_keep = array();

    // Download files startring from 6 days ago to today
    while ($date <= date('Ymd')) {
        echo "Retrieving files from " . $date . "...\n";

        // Get specific URL
        $cfs_date = "cfs." . $date;
        $subfolder = array("6hrly_grib_01/", "6hrly_grib_02/", "6hrly_grib_03/", "6hrly_grib_04/", "monthly_grib_01/", "monthly_grib_02/", "monthly_grib_03/", "monthly_grib_04/", "time_grib_01/", "time_grib_02/", "time_grib_03/", "time_grib_04/");
        
        // Repeat for each subfolder
        foreach ($subfolder as $folder) {
            // Get current folder to download files
            $path = "cfs/" . $cfs_date . "/00/" . $folder;

            // Mount full directory to download
            $source = $BASE_URL . $path;

            // Calls url_exists to check if $source is a valid URL
            echo "Checking if " . $source . " is a valid URL... ";
            if (url_exists($source)) {
                // Check if $path exists locally, if not creates it
                if (!file_exists($path)) {
                    echo "Creating local dir " . $path . "\n";
                    mkdir($path, 0777, true);
                }

                // Add folder where files will be downloaded to $folders_to_keep if it isn't already in the array
                if (!in_array("cfs/" . $cfs_date, $folders_to_keep)) {
                    array_push($folders_to_keep, "cfs/" . $cfs_date);
                }

                // Calls get_text and allocates the information in $matches
                // The filenames to download will be allocated in $matches[2][0..n]
                echo "Getting list of files to download...\n";
                $matches = array();
                preg_match_all("/(a href\=\")([^\?\"]*)(\")/i", get_text($source), $matches);

                // Removes $matches[2][0], which represents the parent directory
                // All filenames will be allocated in $files_to_download and thus can be accessed using $files_to_download[0..n]
                $files_to_download = array_splice($matches[2], 1);

                foreach ($files_to_download as $file) {
                    // Check if $file exists locally, if not downloads it
                    if (!file_exists($path . $file)) {
                        echo "Downloading " . $file . "... ";
                        file_put_contents($path . $file, fopen($source . $file, 'r'));
                        echo "OK!\n";
                    } else {
                        echo $file . " already exists in folder, skipped\n";
                    }
                }
            } 
        }

        // Get next day
        $date = date('Ymd', strtotime($date . ' + 1 day'));
        echo "\n";
    }

    // Get all folders in local dir cfs/
    $folders = glob("cfs/*");
    echo "Folders in cfs/: \n";
    print_r($folders);

    // Deletes all folders that are not specified in $folders_to_keep
    echo "Folders to keep: \n";
    print_r($folders_to_keep);
    foreach ($folders as $folder) {
        if (!in_array($folder, $folders_to_keep)) {
            echo "Deleting $folder... ";
            delete_directory($folder);
            echo "OK!\n";
        }
    }

    // Function that retrieves a single string with all files from $source
    function get_text($filename) {
        $content = "";
        $fp_load = fopen("$filename", "rb");
        if ($fp_load) {
            while (!feof($fp_load)) {
                $content .= fgets($fp_load, 8192);
            }
            fclose($fp_load);
            return $content;
        }
    }

    // Function to check if a URL is valid
    function url_exists($url) {
        $handle = curl_init($url);
        curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
        // Set time out => 120s
        curl_setopt($handle, CURLOPT_TIMEOUT, 120);

        $response = curl_exec($handle);
        $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        if($httpCode >= 200 && $httpCode < 400) {
            echo "VALID!\n";
            return true;
        } else {
            if ($httpCode == 0) echo "TIMEOUT!\n";
            else echo "INVALID!\n";
            return false;
        }

        curl_close($handle);
    }

    // Function to delete local dir
    function delete_directory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!delete_directory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }
?>