<?php
    /**
     *      Esse script tem por objetivo conectar no FTP da NOAA e baixar todos os arquivos de previsão de clima de $FILE_DURATION dias passados.
     *      Como os arquivos são grandes, o script também fica responsãvel pela limpeza do HD, deletando todos os arquivos anteriores a $FILE_DURATION dias.
     *      Para garantir eficiência e economizar banda, o script verifica se o arquivo já existe em ambiente local antes de realizar o download.
     */


    $FTP_HOST = "nomads.ncdc.noaa.gov";
    $FTP_USER = 'anonymous';
    $FTP_PASS = '';
    $FTP_PASSIVE_MODE = true;
    $conn = connect($FTP_HOST, $FTP_USER, $FTP_PASS, $FTP_PASSIVE_MODE);


    $FILE_DURATION = 5;


    // *** Pega a data atual - $FILE_DURATION
    $data = date('Y-m-d', strtotime('-' . $FILE_DURATION . ' day'));
    // *** Baixa os arquivos a partir de data atual - $FILE_DURATION, inclusive
    while ($data <= date('Y-m-d')) {
        /* 
        * [0] - ano
        * [1] - mes
        * [2] - dia
        */
        $dataVetor = explode('-', $data);

        // *** Monta o caminho para a pasta
        $pathInicial = '/GFS/Grid4/';
        $pathMes = $dataVetor[0] . $dataVetor[1] . "/";
        $pathDia = $dataVetor[0] . $dataVetor[1] . $dataVetor[2] . "/";
        $folder = $pathInicial . $pathMes . $pathDia;

        // *** Se existe a pasta, baixa os arquivos
        if (is_dir("ftp://" . $FTP_HOST . $folder) == true) {
            //lista todos os arquivos de uma determinada pasta
            $arquivos = ftp_nlist($conn, $folder);

            foreach ($arquivos as $path) {
                $partes = explode('/', $path);
                $nome = $partes[count($partes)-1];

                // *** Verfica se a pasta existe, se não, cria 
                if (!file_exists($pathMes . $pathDia)) {
                    mkdir($pathMes . $pathDia, 0777, true);
                }

                // *** Verfica se o arquivo existe, se não faz o download
                if (!file_exists($pathMes . $pathDia . $nome)) {
                    downloadFile($conn, $pathMes . $pathDia . $nome, $path);
                } else {
                    echo $pathMes . $pathDia . $nome . " encontrado, o download nao foi efetuado \n";
                }

            }

        } else {
            echo $folder . " nao existe no servidor \n";
        }

        $data = date('Y-m-d', strtotime($data . ' + 1 day'));
    }
    
    //encerra conexão FTP com o servidor
    closeConnection($conn);

    // *** Pega a data atual - $FILE_DURATION
    $data = date('Y-m-d', strtotime('-' . $FILE_DURATION . ' day'));
    // *** Deleta os arquivos anteriores a data atual - $FILE_DURATION, exclusive
    do {
        $data = date('Y-m-d', strtotime($data . ' - 1 day'));
        
        $dataVetor = explode('-', $data);
        $pathMes = $dataVetor[0] . $dataVetor[1] . "/";
        $pathDia = $dataVetor[0] . $dataVetor[1] . $dataVetor[2] . "/";

        echo "Verificando se existe o diretorio " . $pathMes . $pathDia . "\n";

        if (file_exists($pathMes . $pathDia)) {
            echo "Deletando pasta ".$pathMes . $pathDia."\n";
            deleteDirectory($pathMes . $pathDia);
            $continua = true;
        } else {
            echo "Nao encontrado!\n";
            $continua = false;
        }  
    } while ($continua);



    function connect($server, $ftpUser, $ftpPassword, $isPassive = false){
     
        // *** Set up basic connection
        $connectionId = ftp_connect($server);
     
        // *** Login with username and password
        $loginResult = ftp_login($connectionId, $ftpUser, $ftpPassword);
     
        // *** Sets passive mode on/off (default off)
        ftp_pasv($connectionId, $isPassive);
     
        // *** Check connection
        if ((!$connectionId) || (!$loginResult)) {
            var_dump('FTP connection has failed!');
            var_dump('Attempted to connect to ' . $server . ' for user ' . $ftpUser);
            return false;
        } else {
            var_dump('Connected to ' . $server . ', for user ' . $ftpUser);
            $loginOk = true;
            return $connectionId;
        }
    }

    /* 
    * $conn - Conexão com FTP
    * $nome - Nome que o arquivo receberá no meu ambiente
    * $path - caminho para chegar no arquivo no servidor 
    */
    function downloadFile($conn, $nome, $path){
        echo "Iniciando download de $nome\n";
        ftp_get($conn,  $nome , $path , FTP_BINARY);
        echo "Download finalizado\n";
    }

    function closeConnection($conn){
        ftp_close($conn);
    }

    function deleteDirectory($dir) {
        if (!file_exists($dir)) {
            return true;
        }

        if (!is_dir($dir)) {
            return unlink($dir);
        }

        foreach (scandir($dir) as $item) {
            if ($item == '.' || $item == '..') {
                continue;
            }

            if (!deleteDirectory($dir . DIRECTORY_SEPARATOR . $item)) {
                return false;
            }

        }

        return rmdir($dir);
    }

?>