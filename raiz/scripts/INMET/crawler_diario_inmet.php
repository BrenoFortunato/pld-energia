<?php 
	// Create curl resource 
	$ch = curl_init(); 

	// Set url 
	curl_setopt($ch, CURLOPT_URL, "http://www.inmet.gov.br/sonabra/maps/pg_mapa.php"); 

	// Return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

	// $output contains the output string 
	$output = curl_exec($ch); 

	// Close curl resource to free up system resources 
	curl_close($ch);

	// Puts the info we want on $matches using regex
	$er = '/var html = \'<div style="width:240px"><font size="1" face="Arial" color="black"><b>(.*);[\n]/';
	$matches = array();
	$t = preg_match_all($er, $output, $matches);

	// Set timezone and save current hour on $hour
	date_default_timezone_set('America/Sao_Paulo');
	$hour = date('H');

	// If $hour >= 13 then $filename should be today's date, else yesterday's
	if ($hour >= 13) {
		$fileName = "" . date("dmY") . ".csv";
	} else {
		$fileName = "" . date("dmY", strtotime('-1 days')) . ".csv";
	}

	// Check if exists a file with $filename, if not, creates it and set header
    if (!file_exists($fileName)) {
		$header = array("estacao;codigo;registro;tempMax;tempMin;umidade;pressao;precipitacao;ventoDir;ventoVel;foto;abertaEm;latitude;longitude;altitude");
		writeToCSV($header, $fileName);
	}

	// Creates the array from which the csv will be filled
	$arrayToInsert = array();

	foreach ($matches[0] as $match) {
		$match = mb_convert_encoding($match, 'utf-8', 'ISO-8859-1');

		// Remove unnecessary information
		$match = str_replace('var html = \'<div style="width:240px"><font size="1" face="Arial" color="black"><b><b>', '', $match);
		$match = str_replace('<b>', '', $match);
		$match = str_replace('</b>', '', $match);
		$match = str_replace('</td><td width=45>', '', $match);
		$match = str_replace('</font><\/div>\';', '', $match);
		$match = str_replace('Gráfico</td></tr></table>', '', $match);

		// Explode $match on $valores which will contain the info we want  
		$valores = explode('<br>', $match);

		// Creates some variables
		$estacao = '';
		$codigo = '';
		$registro = '';
		$tempMax = '';
		$tempMin = '';
		$umidade = '';
		$pressao = '';
		$precipitacao = '';
		$ventoDir = '';
		$ventoVel = '';
		$foto = '';
		$abertaEm = '';
		$latitude = '';
		$longitude = '';
		$altitude = '';

		// Interates over $valores filtering the info on its respective variables
		foreach ($valores as $valor) {
			if (strpos($valor, 'Estação') !== false) {
				$estacao = $valor;
				$estacao = str_replace('Estação: ', '', $estacao);
			}

			if (strpos($valor, 'Código OMM') !== false) {
				$codigo = $valor;
				$codigo = str_replace('Código OMM: ', '', $codigo);
			}

			if (strpos($valor, 'Registro') !== false) {
				$registro = $valor;
				$registro = str_replace('Registro: ', '', $registro);
				$registro = str_replace(' UTC', '', $registro);
			}

			if (strpos($valor, 'Temp. Max.') !== false) {
				$tempMax = $valor;
				$tempMax = str_replace('Temp. Max.: ', '', $tempMax);
				$tempMax = str_replace(' ºC', '', $tempMax);
			}

			if (strpos($valor, 'Temp. Min.') !== false) {
				$tempMin = $valor;
				$tempMin = str_replace('Temp. Min.: ', '', $tempMin);
				$tempMin = str_replace(' ºC', '', $tempMin);
			}

			if (strpos($valor, 'Umidade') !== false) {
				$umidade = $valor;
				$umidade = str_replace('Umidade: ', '', $umidade);
				$umidade = str_replace('%', '', $umidade);
			}

			if (strpos($valor, 'Pressão') !== false) {
				$pressao = $valor;
				$pressao = str_replace('Pressão: ', '', $pressao);
				$pressao = str_replace(' hPa', '', $pressao);
			}

			if (strpos($valor, 'Precipitação') !== false) {
				$precipitacao = $valor;
				$precipitacao = str_replace('Precipitação: ', '', $precipitacao);
				$precipitacao = str_replace(' mm', '', $precipitacao);
			}

			if (strpos($valor, 'Vento Dir') !== false) {
				$ventoDir = $valor;
				$ventoDir = str_replace('Vento Dir: ', '', $ventoDir);
				$ventoDir = str_replace(' º', '', $ventoDir);
			}

			if (strpos($valor, 'Vento Vel') !== false) {
				$ventoVel = $valor;
				$ventoVel = str_replace('Vento Vel: ', '', $ventoVel);
				$ventoVel = str_replace(' m/s', '', $ventoVel);
			}

			if (strpos($valor, 'alt=Foto') !== false) {
				$foto = $valor;
				$foto = str_replace('<a href=', '', $foto);
				$foto = explode(' ', $foto)[0];
			}

			if (strpos($valor, 'Aberta em') !== false) {
				$abertaEm = $valor;
				$abertaEm = str_replace('Aberta em: ', '', $abertaEm);
			}

			if (strpos($valor, 'Latitude') !== false) {
				$latitude = $valor;
				$latitude = str_replace('Latitude: ', '', $latitude);
				$latitude = str_replace('º', '', $latitude);
			}

			if (strpos($valor, 'Longitude') !== false) {
				$longitude = $valor;
				$longitude = str_replace('Longitude: ', '', $longitude);
				$longitude = str_replace('º', '', $longitude);
			}

			if (strpos($valor, 'Altitude') !== false) {
				$altitude = $valor;
				$altitude = str_replace('Altitude: ', '', $altitude);
				$altitude = str_replace(' metros', '', $altitude);
				$altitude = str_replace("\n", '', $altitude);
			}
		}

		// Creates an unique string with all the info separating them with ;
		$newLine = '' . $estacao . ';' . $codigo . ';' . $registro . ';' . $tempMax . ';' . $tempMin . ';' . $umidade . ';' . $pressao . ';' . $precipitacao . ';' . $ventoDir . ';' . $ventoVel . ';' . $foto . ';' . $abertaEm . ';' . $latitude . ';' . $longitude . ';' . $altitude;
		
		// Push the string into $arrayToInsert, this will be a line on the csv
		array_push($arrayToInsert, $newLine);
	}

	// Writes all the lines on the csv
	writeToCSV($arrayToInsert, $fileName);

	//TODO: Escrever direto dentro da hierarquia de pastas no .CSV do arquivo. 


	// Function to write on a csv
	function writeToCSV($lineToInsert, $fileName){	
		$file = fopen($fileName, "a");

		foreach ($lineToInsert as $line) {
			fputcsv($file, explode(';', $line));
		}

		fclose($file);
	}

?>



